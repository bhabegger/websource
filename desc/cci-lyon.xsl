<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" />

    <!--
    <div id="content-entreprise-middle" class="is-md-12">
        <div id="entreprise-nom">
            <h3>FINSECUR   </h3>
        </div>
        <ul id="entreprise-personnes">

        </ul>
        <div id="entreprise-adresse">

            9  allée(s) DES GINKGOS
            <br>


            69500


            BRON

            <br>

            Tél :

            <a href="tel:0472159765">0472159765</a>

        </div>
        <div id="entreprise-contacts">
            Email :

            <br>

        </div>
        <div id="entreprise-activite">
            <b>Activité</b>
            DEV FAB MATERIEL MECANIQUE
        </div>
        <div id="entreprise-details">
            <span class="bold">Année de création</span>2016
            <br>
            <span class="bold">Statut</span> DELEG. REGIONALE ENTR. A SIEGE EXTERIEUR
            <br><span class="bold">Forme juridique - Capital</span> SOCIETE PAR ACTIONS SIMPLIFIEE -
            <br>
            <span class="bold">Effectif</span>15 A 19 SALARIES
            <br>
            <span class="bold">Chiffre d'affaire</span>
            <br>
            <span class="bold">n° SIRET</span>35058939600158
            <br>
            <span class="bold">n° APE</span>26.30Z
            <br>
        </div>
    </div>
    -->
    <xsl:template match="/">
        <xsl:apply-templates select="//div[@id='content-entreprise-middle']" />
    </xsl:template>

    <xsl:template match="div[@id='content-entreprise-middle']">
        <company>
            <name><xsl:value-of select="normalize-space(.//div[@id='entreprise-nom']/h3)" /></name>
            <activite><xsl:value-of select="normalize-space(.//div[@id='entreprise-activite']/b/following-sibling::text())" /></activite>
            <address>
                <xsl:apply-templates select=".//div[@id='entreprise-adresse']" mode="address" />
            </address>
            <telephone>
                <xsl:apply-templates select=".//div[@id='entreprise-adresse']/a" />
            </telephone>

            <xsl:apply-templates select=".//div[@id='entreprise-details']/span" mode="details" />
        </company>
    </xsl:template>

    <xsl:template match="text()" mode="address">
        <xsl:if test="not(contains(.,'Tél'))">
            <xsl:value-of select="normalize-space(.)" />
        </xsl:if>
    </xsl:template>

    <xsl:template match="br" mode="address">
        <xsl:text>&#10;</xsl:text>
    </xsl:template>

    <xsl:template match="a" mode="address"></xsl:template>

    <xsl:template match="span[text()='Année de création']" mode="details">
        <created><xsl:value-of select="normalize-space(following-sibling::text()[1])" /></created>
    </xsl:template>

    <xsl:template match="span[text()='Statut']" mode="details">
        <status><xsl:value-of select="normalize-space(following-sibling::text()[1])" /></status>
    </xsl:template>

    <xsl:template match='span[text()="Chiffre d&#39;affaire"]' mode="details">
        <turnover><xsl:value-of select="normalize-space(following-sibling::text()[1])" /></turnover>
    </xsl:template>

    <xsl:template match="span[text()='Forme juridique - Capital']" mode="details">
        <forme><xsl:value-of select="normalize-space(substring-before(following-sibling::text()[1],'-'))" /></forme>
        <capital><xsl:value-of select="normalize-space(substring-after(following-sibling::text()[1],'-'))" /></capital>
    </xsl:template>

    <xsl:template match="span[text()='n° SIRET']" mode="details">
        <siret><xsl:value-of select="normalize-space(following-sibling::text()[1])" /></siret>
    </xsl:template>

    <xsl:template match="span[text()='n° APE']" mode="details">
        <ape><xsl:value-of select="normalize-space(following-sibling::text()[1])" /></ape>
    </xsl:template>

    <xsl:template match="text()" mode="details" />
</xsl:stylesheet>